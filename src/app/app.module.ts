import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { SettingsComponent } from './components/settings/settings.component';
import { RealtimeComponent } from './components/realtime/realtime.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { SiteLineRowComponent } from './components/site-line-row/site-line-row.component';
import { DepartureDisplayComponent } from './components/departure-display/departure-display.component';
import { LineBadgeComponent } from './components/line-badge/line-badge.component';
import { SiteDeviationComponent } from './components/site-deviation/site-deviation.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    SettingsComponent,
    RealtimeComponent,
    PageNotFoundComponent,
    SiteLineRowComponent,
    DepartureDisplayComponent,
    LineBadgeComponent,
    SiteDeviationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
