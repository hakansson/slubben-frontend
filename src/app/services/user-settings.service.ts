import { Injectable } from '@angular/core';
import { Site } from '../model/site';
import { Deviation } from '../model/deviation';

const SITE_KEY = "user-selected-site";
const SITE_DEVIATIONS_KEY = "user-dismissed-site-deviations";
const API_URL_KEY = "user-api-url"

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {
  private site: Site = null;
  private dismissedSiteDeviations: Deviation[] = []
  private apiURL: String = "http://localhost:8080"

  constructor() {
    const savedSite = localStorage.getItem(SITE_KEY)
    if (savedSite !== null) {
      try {
        const json = JSON.parse(savedSite)
        this.site = json
      } catch (err) {
        console.error("Failed to parse saved site", err)
      }
    }
    const dismissedSites = localStorage.getItem(SITE_DEVIATIONS_KEY)
    if (dismissedSites !== null) {
      try {
        const parsed = JSON.parse(dismissedSites)
        this.dismissedSiteDeviations = parsed;
      } catch (err) {
        console.log("Failed to parse dismissed site deviations", err)
      }
    }

    const savedAPI = localStorage.getItem(API_URL_KEY)
    if (savedAPI !== null) {
      this.apiURL = savedAPI;
    }
  }

  getSite(): Site {
    return this.site;
  }

  hasSite(): boolean {
    return this.site !== null;
  }

  saveSite(site: Site) {
    this.site = site;
    localStorage.setItem(SITE_KEY, JSON.stringify(this.site));
  }

  deleteSite() {
    this.site = null;
    localStorage.removeItem(SITE_KEY);
  }

  dismissSiteDeviation(deviation: Deviation) {
    this.dismissedSiteDeviations.push(deviation)
    localStorage.setItem(SITE_DEVIATIONS_KEY, JSON.stringify(this.dismissedSiteDeviations))
  }

  isSiteDeviationDismissed(deviation: Deviation): boolean {
    return this.dismissedSiteDeviations.find((dev) => {
      return dev.importance == deviation.importance &&
      dev.consequence == deviation.consequence &&
      dev.text == deviation.text
    }) !== undefined
  }

  resetDismissedSiteDeviations() {
    this.dismissedSiteDeviations = [];
    localStorage.setItem(SITE_DEVIATIONS_KEY, JSON.stringify(this.dismissedSiteDeviations));
  }

  saveURL(api: String) {
    this.apiURL = api
    localStorage.setItem(API_URL_KEY, api.toString())
  }

  API(): String {
    return this.apiURL;
  }
}
