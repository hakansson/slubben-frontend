import { TestBed } from '@angular/core/testing';

import { SiteLookupService } from './site-lookup.service';

describe('SiteLookupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SiteLookupService = TestBed.get(SiteLookupService);
    expect(service).toBeTruthy();
  });
});
