import { Injectable } from '@angular/core';
import { Site } from '../model/site';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';
import { UserSettingsService } from './user-settings.service';

@Injectable({
  providedIn: 'root'
})
export class SiteLookupService {

  constructor(private http: HttpClient, private userSettings: UserSettingsService) {}

  searchForSite(query: String): Observable<Site[]> {
    const url = `${this.userSettings.API()}/search?query=${query}`
    return this.http.get<Site[]>(url)
  }

}
