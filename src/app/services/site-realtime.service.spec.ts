import { TestBed } from '@angular/core/testing';

import { SiteRealtimeService } from './site-realtime.service';

describe('SiteRealtimeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SiteRealtimeService = TestBed.get(SiteRealtimeService);
    expect(service).toBeTruthy();
  });
});
