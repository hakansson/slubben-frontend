import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SiteDepartures } from '../model/siteDepartures';
import { HttpClient } from '@angular/common/http';
import { UserSettingsService } from './user-settings.service';

@Injectable({
  providedIn: 'root'
})
export class SiteRealtimeService {

  constructor(private http: HttpClient, private userSettings: UserSettingsService) { }

  getLinesForSite(siteID: Number): Observable<SiteDepartures> {
    const url = `${this.userSettings.API()}/departures/${siteID}`
    return this.http.get<SiteDepartures>(url)
  }
}
