import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLineRowComponent } from './site-line-row.component';

describe('SiteLineRowComponent', () => {
  let component: SiteLineRowComponent;
  let fixture: ComponentFixture<SiteLineRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteLineRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLineRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
