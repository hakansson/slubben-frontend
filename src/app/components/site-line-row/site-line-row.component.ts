import { Component, OnInit, Input } from '@angular/core';
import { SiteLine } from 'src/app/model/site-line';

@Component({
  selector: 'app-site-line-row',
  templateUrl: './site-line-row.component.html',
  styleUrls: ['./site-line-row.component.scss']
})
export class SiteLineRowComponent implements OnInit {

  @Input() line: SiteLine;
  @Input() rowIndex: number;

  constructor() { }

  ngOnInit() {
  }

}
