import { Component, OnInit, Input } from '@angular/core';
import { Deviation } from 'src/app/model/deviation';
import { UserSettingsService } from 'src/app/services/user-settings.service';

@Component({
  selector: 'app-site-deviation',
  templateUrl: './site-deviation.component.html',
  styleUrls: ['./site-deviation.component.scss']
})
export class SiteDeviationComponent implements OnInit {

  @Input() deviation: Deviation;
  private isDismissed: boolean = false;

  constructor(private userSettings: UserSettingsService) { }

  ngOnInit() {
  }

  dismiss() {
    if (this.isDismissed) {
      console.log("Already dismissed")
      return
    }
    if (confirm("Markera som läst")) {
      this.isDismissed = true;
      this.userSettings.dismissSiteDeviation(this.deviation)
    }
  }
}
