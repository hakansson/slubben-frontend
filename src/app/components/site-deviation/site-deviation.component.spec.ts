import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteDeviationComponent } from './site-deviation.component';

describe('SiteDeviationComponent', () => {
  let component: SiteDeviationComponent;
  let fixture: ComponentFixture<SiteDeviationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteDeviationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteDeviationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
