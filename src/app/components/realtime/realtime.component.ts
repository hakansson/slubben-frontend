import { Component, OnInit } from '@angular/core';
import { SiteLine } from 'src/app/model/site-line';
import { LineType } from 'src/app/model/line-type';
import { SiteRealtimeService } from 'src/app/services/site-realtime.service';
import { UserSettingsService } from 'src/app/services/user-settings.service';
import { Deviation } from 'src/app/model/deviation';
import { Departure } from 'src/app/model/departure';
import { environment } from 'src/environments/environment';

let LINES: SiteLine[] = [
  {
    type: LineType.METRO_GREEN,
    number: "19",
    destination: "Globen",
    direction: 1,
    departures: [
      {
        planned: new Date(),
        expected: new Date(),
        hasExpected: true,
        deviations: []
      },
      {
        planned: new Date((new Date()).getTime() + 10*60000),
        expected: new Date((new Date()).getTime() + 10*60000),
        hasExpected: true,
        deviations: [
          {
            text: "Något är försenat",
            importance: 3,
            consequence: null,
          }
        ]
      }
    ]
  },
  {
    type: LineType.METRO_GREEN,
    number: "19",
    destination: "Hagsätra",
    direction: 2,
    departures: [
      {
        planned: new Date((new Date()).getTime() + 2*60000),
        expected: new Date((new Date()).getTime() + 2*60000),
        hasExpected: true,
        deviations: []
      }
    ]
  }
]

let DEVIATIONS: Deviation[] = [
  {
    importance: 4,
    text: "Detta är ett test",
    consequence: ""
  }
]

@Component({
  selector: 'app-realtime',
  templateUrl: './realtime.component.html',
  styleUrls: ['./realtime.component.scss']
})
export class RealtimeComponent implements OnInit {

  lines: SiteLine[] = [];
  deviations: Deviation[] = [];

  constructor(
    private userSettings: UserSettingsService,
    private siteRealtime: SiteRealtimeService
    ) { }

  ngOnInit() {
    setInterval(() => {
      this.pruneDismissedDeviations();
    }, 1000) // Every second
    setInterval(() => {
      this.pruneLinesThatLeft();
    }, 10 * 1000) // Every 10 seconds
    if (environment.production) {
      this.updateLines();
      setInterval(() => {
        this.updateLines();
      }, 1000 * 60 * 5) // every 5 minutes
    } else {
      this.lines = LINES;
      this.setDeviations(DEVIATIONS)
    }
  }

  updateLines() {
    if (!this.userSettings.hasSite()) {
      console.log("No site selected, skipping update")
      return
    }
    console.log("Updating lines");
    const siteID = this.userSettings.getSite().id
    this.siteRealtime.getLinesForSite(siteID).subscribe((res) => {
      this.lines = res.lines
      this.setDeviations(res.siteDeviations)
    });
  }

  pruneLinesThatLeft() {
    console.log("Pruning old lines")
    const probablyLeft = new Date(new Date().getTime() - 40 * 1000)
    const depFilter = (dep: Departure) => {
      let timeToDeparture = new Date(dep.planned);
      if (dep.hasExpected) {
        timeToDeparture = new Date(dep.expected);
      }
      return timeToDeparture >= probablyLeft;
    }
    this.lines.forEach((line => {
      line.departures = line.departures.filter(depFilter);
    }));
    this.lines = this.lines.filter((line) => { return line.departures.length > 0 })
  }

  setDeviations(newDevs: Deviation[]) {
    this.deviations = newDevs.filter((dev) => {
      return !this.userSettings.isSiteDeviationDismissed(dev)
    });
  }

  pruneDismissedDeviations() {
    this.deviations = this.deviations.filter((dev) => {
      return !this.userSettings.isSiteDeviationDismissed(dev)
    });
  }
}
