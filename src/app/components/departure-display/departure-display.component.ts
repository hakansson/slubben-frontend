import { Component, OnInit, Input } from '@angular/core';
import { Departure } from 'src/app/model/departure';

@Component({
  selector: 'app-departure-display',
  templateUrl: './departure-display.component.html',
  styleUrls: ['./departure-display.component.scss']
})
export class DepartureDisplayComponent implements OnInit {

  text: String = "";

  @Input() departure: Departure;

  constructor() { }

  ngOnInit() {
    this.updateText();
    setInterval(() => {
      this.updateText();
   }, 500);
  }

  updateText() {
    // 45 seconds from now
    const now = new Date();
    const consideredNow = new Date(now.getTime() + 30*1000);
    const probablyLeft = new Date(now.getTime() - 30*1000);
    let timeToDeparture = new Date(this.departure.planned);
    if (this.departure.hasExpected) {
      timeToDeparture = new Date(this.departure.expected);
    }
    if (timeToDeparture < probablyLeft) {
      this.text = "Avgått"
    } else if (timeToDeparture < consideredNow) {
      this.text = "Nu"
    } else {
      const minutesUntilDeparture = (timeToDeparture.getTime() - now.getTime())/60000
      this.text = minutesUntilDeparture.toFixed(0) + " min";
    }
  }

  onClick() {
    if (this.departure.deviations.length <= 0) {
      // Do nothing
      return
    }

    let message = ""
    this.departure.deviations.forEach((dev) => {
      message += dev.text + "\n\n"
    });

    alert(message.trim())
  }

}
