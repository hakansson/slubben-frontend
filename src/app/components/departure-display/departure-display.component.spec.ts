import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartureDisplayComponent } from './departure-display.component';

describe('DepartureDisplayComponent', () => {
  let component: DepartureDisplayComponent;
  let fixture: ComponentFixture<DepartureDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartureDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartureDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
