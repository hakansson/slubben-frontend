import { Component, OnInit, ViewChild } from '@angular/core';
import { UserSettingsService } from 'src/app/services/user-settings.service';
import { SiteLookupService } from 'src/app/services/site-lookup.service';
import { Site } from 'src/app/model/site';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  @ViewChild("apiURLInput") apiUrlInput;

  private searchResults: Site[] = [];

  constructor(
    public userSettings: UserSettingsService,
    private siteLookup: SiteLookupService) { }

  ngOnInit() {
    this.apiUrlInput.nativeElement.value = this.userSettings.API()
  }

  searchForSite(query: String) {
    console.log("Search for:", query);
    if (query.trim() == "") {
      return;
    }

    this.siteLookup.searchForSite(query).subscribe((sites) => {
      console.log(sites)
      this.searchResults = sites;
    })
  }

  selectSite(site: Site) {
    console.log("Selected site", site);
    this.searchResults = [];
    this.userSettings.saveSite(site);
  }

  removeSite() {
    console.log("Removing site");
    this.userSettings.deleteSite();
  }

  resetDismissedSiteDeviations() {
    console.log("Resetting dismissed site deviations")
    this.userSettings.resetDismissedSiteDeviations();
  }

  saveAPI(url: String) {
    this.userSettings.saveURL(url)
  }

}
