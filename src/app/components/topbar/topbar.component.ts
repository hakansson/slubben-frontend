import { Component, OnInit } from '@angular/core';
import { UserSettingsService } from 'src/app/services/user-settings.service';
import { Site } from 'src/app/model/site';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  public time = new Date();

  constructor(public userSettings: UserSettingsService) { }

  ngOnInit() {
    this.initializeClock();

    //this.userSettings.site = new Site("2", "Foobar");
  }

  initializeClock() {
    setInterval(() => {
      this.time = new Date();
   }, 1000);
  }

}
