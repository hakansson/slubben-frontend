import { Component, OnInit, Input } from '@angular/core';
import { SiteLine } from 'src/app/model/site-line';
import { LineType } from 'src/app/model/line-type';

@Component({
  selector: 'app-line-badge',
  templateUrl: './line-badge.component.html',
  styleUrls: ['./line-badge.component.scss']
})
export class LineBadgeComponent implements OnInit {

  @Input() line: SiteLine;

  private bgColor = "blue";
  private txtColor = "white";

  constructor() { }

  ngOnInit() {
    switch (this.line.type) {
      case LineType.METRO_RED:
        this.bgColor = "red";
        this.txtColor = "white";
        break;
      case LineType.METRO_GREEN:
        this.bgColor = "green";
        this.txtColor = "white";
        break;
      case LineType.METRO_BLUE:
        this.bgColor = "blue";
        this.txtColor = "white";
        break;
      case LineType.BUS:
        this.bgColor = "blue";
        this.txtColor = "lime";
        break;
      case LineType.SHIP:
        // TODO: custom colors
        break;
      case LineType.TRAIN:
        break;
      case LineType.TRAM:
        break;
      default:
        break;
    }
  }

  setColors() {
    const styles = {
      'background-color': this.bgColor,
      'color': this.txtColor
    };
    return styles;
  }

}
