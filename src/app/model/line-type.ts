export enum LineType {
    METRO_RED = "METRO_RED",
    METRO_GREEN = "METRO_GREEN",
    METRO_BLUE = "METRO_BLUE",
    BUS = "BUS",
    TRAIN = "TRAIN",
    TRAM = "TRAIM",
    SHIP = "SHIP"
}