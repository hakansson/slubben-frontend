export interface RawResponse<T> {
    StatusCode: Number
    Message: String
    ExecutionTime: Number
    ResponseData: T
}
