export interface Site {
    id: Number;
    name: String;
    displayName: String;
}
