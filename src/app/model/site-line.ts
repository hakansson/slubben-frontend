import { LineType } from './line-type';
import { Departure } from './departure';

export interface SiteLine {
    type: LineType
    number: String
    destination: String
    direction: Number
    departures: Departure[]
}
