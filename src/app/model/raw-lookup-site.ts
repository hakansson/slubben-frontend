export interface RawLookupSite {
    Name: String
    SiteId: Number
    Type: String
    X: String
    Y: String
}
