import { SiteLine } from './site-line';
import { Deviation } from './deviation';

export interface SiteDepartures {
    lines: SiteLine[]
    siteDeviations: Deviation[]
}