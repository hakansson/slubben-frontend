import { Deviation } from './deviation';

export interface Departure {
    planned: Date
    expected: Date
    hasExpected: boolean
    deviations: Deviation[]
}
