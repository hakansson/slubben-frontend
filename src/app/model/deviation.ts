export interface Deviation {
    text: String
    importance: Number // higher is more important
    consequence: String
}