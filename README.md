# Slubben Frontend
The backend part of _Slubben_. The frontend can be found at: https://gitlab.com/hakansson/slubben-frontend

The frontend is created with Angular 7.2.16, for easy development, use [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

When running in development mode, dummy data is used for the realtime table. This can easily be overridden by either running in Production mode (`--prod`) or to change the logic in the RealtimeComponent (`realtime.component.ts`).

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
